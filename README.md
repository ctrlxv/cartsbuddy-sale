
## Setup CartsBuddy Sale Web Site


If you have experience with Firebase project, directly checkout and start coding. Otherwise please refer to below steps.

#### Checkout project
1. Clone the project by getting the URL from **clone** on the left upper corner of **"cartsbuddy-sale"** repository.
2. Open a git bash in a directory, paste the previously copied clone URL and run it. 

#### Build Project
3. Update NPM dependencies with **"npm install"**
4. Build the project by running **"npm run build"**


#### Run Project
5. Run the project with command **"npm run start"**
6. Deployed URL will be shown in the console. This is usually "http://localhost:5000"


#### Deploy to Staging
7. Merge the feature branch to **develop** branch.
8. Check the changes at **https://staging.cartsbuddy.com/**


#### Deploy to Production
9. Merge the changes to **master** branch.
10. Go to related Bitbucket pipeline and confirm deployment to production by pressing **"Deploy"** button.
11. Verify the changes at **https://cartsbuddy.com/**
